## 06/04/2021

-[] Finish GLS in the VMAT
	- 91_NG_cur etc missing from master sheet for GLS
	- G7_Q0_cmd_CL/OP missing from GLS sigs
	- 00_UPS_bat_low missing from GLS sigs
	- 00_UPS_fault missing from GLS sigs
-[x] VMAT phase 2 planning
	-[x] Contact projects to set up a meeting with anyone who touches signals
-[] Big diff script of all remaining sites TT

Plan is, try and charge through GLS. Then, whoever is done first out of Dan and I on their site
will start work on the big diff script, the other supporting. Either way I will sneak in ~1h 
of VMAT phase2 planning


## 07/04/2021

Same as Day before.

## 08/04/2021

-[x] Send GLS email to Dave
-[x] Send email to signals users
-[X] start other site vmat entry


## 09/04/2021

-[] Get working on Roundponds
-[] Get big differ for totaltags
-[] Organise wiring manager with project engineers
